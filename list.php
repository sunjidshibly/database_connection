<html>
    <head>
        <meta charset="UTF-8">
        <title>Database connection</title>
        <style>
           *{margin: 0px auto;}  
        </style>
    </head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <body>
        <?php
   include_once 'book.php';
    $book = new Book();
    $books=$book->index();
?>
        <table style="width:50%" border="1">
  <tr>
    <th>ID</th>
    <th>Title</th> 
    <th>Author</th>
  </tr>
   <?php
   
                foreach($books as $book):
                ?>
                 <tr>
                    <td><?php echo $book['book_id'];?></td>
                    
                    <td><?php echo $book['title'];?></td>
                    
                    <td><?php echo $book['author'];?></td>
 
                </tr>
                <?php
                endforeach;
                ?> 
</table>
        
     <?php
     ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    </body>
</html>